package com.atlassian.jira.plugin.ext.subversion.revisions.scheduling.clustersafe;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugin.ext.subversion.MultipleSubversionRepositoryManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.concurrent.GuardedBy;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;

/**
 * Coordinate all the startup information to decide when it is safe to do complicated work.
 *
 * This class copies the approach used by JIRA Agile to solve this problem.  In particular,
 * it watches as the various events occur.  The actual launching, creation of initial data,
 * and scheduling of background tasks is delayed until all of the pieces of the puzzle are
 * in place.  The other components are initialized explicitly by this launcher, though there
 * are other strategies (like using an event to decouple this interaction) that might be
 * better.
 * </p>
 *
 * @since v2.0
 */
public class SchedulerLauncher implements LifecycleAware, InitializingBean, DisposableBean
{
    private static final Logger logger = LoggerFactory.getLogger(SchedulerLauncher.class);
    private static final String PLUGIN_KEY = "com.atlassian.jira.plugin.ext.subversion";

    private final EventPublisher eventPublisher;
    private final SchedulerService schedulerService;
    private final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager;

    @GuardedBy("this")
    private final Set<LifecycleEvent> lifecycleEvents = EnumSet.noneOf(LifecycleEvent.class);

    public SchedulerLauncher(final EventPublisher eventPublisher, final SchedulerService schedulerService, final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager)
    {
        this.eventPublisher = eventPublisher;
        this.schedulerService = schedulerService;
        this.multipleSubversionRepositoryManager = multipleSubversionRepositoryManager;
    }

    @Override
    public void afterPropertiesSet()
    {
        registerListener();
        onLifecycleEvent(LifecycleEvent.AFTER_PROPERTIES_SET);
    }

    /**
     * This is received from SAL after the system is really up and running
     */
    @Override
    public void onStart()
    {
        onLifecycleEvent(LifecycleEvent.LIFECYCLE_AWARE_ON_START);
    }

    /**
     * It is not safe to use Active Objects before this event is received.
     */
    @EventListener
    public void onPluginEnabled(final PluginEnabledEvent event)
    {
        if (PLUGIN_KEY.equals(event.getPlugin().getKey()))
        {
            onLifecycleEvent(LifecycleEvent.PLUGIN_ENABLED);
        }
    }

    @Override
    public void destroy() throws Exception
    {
        unregisterListener();
        unregisterJobRunner();
    }

    /**
     * The latch which ensures all of the plugin/application lifecycle progress is completed before we call
     * {@code launch()}.
     */
    private void onLifecycleEvent(final LifecycleEvent event)
    {
        if (isLifecycleReady(event))
        {
            unregisterListener();
            try
            {
                launch();
            }
            catch (Exception ex)
            {
                logger.error("Unexpected error during launch", ex);
            }
        }
    }

    /**
     * The event latch.
     * <p>
     * When something related to the plugin initialization happens, we call this with
     * the corresponding type of the event.  We will return {@code true} at most once, when the very last type
     * of event is triggered.  This method has to be {@code synchronized} because {@code EnumSet} is not
     * thread-safe and because we have multiple accesses to {@code lifecycleEvents} that need to happen
     * atomically for correct behaviour.
     * </p>
     *
     * @param event the lifecycle event that occurred
     * @return {@code true} if this completes the set of initialization-related events; {@code false} otherwise
     */
    synchronized private boolean isLifecycleReady(final LifecycleEvent event)
    {
        return lifecycleEvents.add(event) && lifecycleEvents.size() == LifecycleEvent.values().length;
    }

    private void launch() throws Exception
    {
        registerJobRunner();
    }

    private void registerListener()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Register SchedulerLauncher in event listener");
        }
        eventPublisher.register(this);
    }

    private void unregisterListener()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Unregister SchedulerLauncher in event listener");
        }
        eventPublisher.unregister(this);
    }

    private void registerJobRunner()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Register Job Runner");
        }

        schedulerService.registerJobRunner(JobRunnerKey.of(PLUGIN_KEY), new UpdateIndexJob(multipleSubversionRepositoryManager));

        final Date firstRun = new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1));
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JobRunnerKey.of(PLUGIN_KEY))
                .withSchedule(Schedule.forInterval(TimeUnit.HOURS.toMillis(1), firstRun))
                .withRunMode(RUN_LOCALLY);

        try
        {
            final JobId jobId = JobId.of(PLUGIN_KEY + "_UpdateSubversionIndex" );
            schedulerService.scheduleJob(jobId, jobConfig);

            if (logger.isInfoEnabled()) {
                logger.info("Successfully scheduled jobId=" + jobId);
            }
        }
        catch (SchedulerServiceException sse)
        {
            throw new RuntimeException("Unable to create job, id: " + JobId.of(PLUGIN_KEY + "_UpdateSubversionIndex" ), sse);
        }
    }

    private void unregisterJobRunner()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Unregister Job Runner");
        }
        schedulerService.unregisterJobRunner(JobRunnerKey.of(PLUGIN_KEY));
    }


    /**
     * Used to keep track of everything that needs to happen before we are sure that it is safe
     * to talk to all of the components we need to use, particularly the {@code SchedulerService}
     * We will not try to initialize until all of them have happened.
     */
    static enum LifecycleEvent
    {
        AFTER_PROPERTIES_SET,
        PLUGIN_ENABLED,
        LIFECYCLE_AWARE_ON_START
    }

}
