package com.atlassian.jira.plugin.ext.subversion.revisions.scheduling.clustersafe;

import com.atlassian.jira.plugin.ext.subversion.MultipleSubversionRepositoryManager;
import com.atlassian.jira.plugin.ext.subversion.revisions.RevisionIndexer;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * Job responsible for indexing subversion repository
 *
 * @since v2.0
 */
public class UpdateIndexJob implements JobRunner {

    private static final Logger logger = LoggerFactory.getLogger(SchedulerLauncher.class);
    private final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager;

    public UpdateIndexJob(final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager) {
        this.multipleSubversionRepositoryManager = multipleSubversionRepositoryManager;
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest) {

        if(logger.isInfoEnabled())
        {
            logger.info("Indexing job started at {}", new Date());
        }

        try
        {

            final RevisionIndexer revisionIndexer = multipleSubversionRepositoryManager.getRevisionIndexer();
            if (revisionIndexer != null)
            {
                revisionIndexer.updateIndex();
            }
            else
            {
                logger.warn("Tried to index changes but SubversionManager has no revision indexer?");
                return JobRunnerResponse.failed("SubversionManager has no revision indexer");
            }
        }
        catch (Exception e)
        {
            logger.error("Error indexing changes: " + e);
            return JobRunnerResponse.failed("Error when indexing repository");
        }


        return JobRunnerResponse.success();
    }

    public String getJobName()
    {
        return "UpdateIndex";
    }
}
