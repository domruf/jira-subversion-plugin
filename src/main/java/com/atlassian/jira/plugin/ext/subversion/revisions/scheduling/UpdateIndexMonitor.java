package com.atlassian.jira.plugin.ext.subversion.revisions.scheduling;


public interface UpdateIndexMonitor {

    void schedule();

}
